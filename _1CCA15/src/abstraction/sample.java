package abstraction;

public class sample extends demo
{
    @Override
    void gettype()
    {
        System.out.println("company name is Intel");
    }

    @Override
    void processor()
    {
        System.out.println("processor is intel core pentrium i3 processor");
    }

    @Override
    void country()
    {
        System.out.println(" Made in U.S.A(united states of america)");
    }

    @Override
    void memory()
    {
        System.out.println("RAM is 8GB");
    }
}
