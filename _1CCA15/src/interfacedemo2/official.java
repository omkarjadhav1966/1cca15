package interfacedemo2;

public class official implements state  {

    @Override
    public void body1() {
        System.out.println("city council = city level");
    }

    @Override
    public void body2() {
        System.out.println("sub city council = taluka level");
    }

    @Override
    public void body3() {
        System.out.println("gram panchayat =village level");
    }
}
