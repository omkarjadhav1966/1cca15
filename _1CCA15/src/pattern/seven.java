package pattern;

public class seven
{
    public static void main(String[] args)
    {
        int lines = 5;
        int star = 1;
        int ch1 = 1;
        for (int i = 0; i < lines; i++)
        {
            for(int j=0;j<star;j++){
                System.out.println(ch1+++"");
            }
            System.out.println();
            star++;
        }
    }
}