package methodoverride;

public class mainapp7
{
    public static void main(String[] args) {
        System.out.println("main method started");
        sample s1=new sample();
        s1.display();
        test t1=new test();
        t1.display();
        System.out.println("main method end" +
                "ed");
    }
}
